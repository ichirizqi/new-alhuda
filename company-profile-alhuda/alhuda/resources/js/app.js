require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:8000/api/'

import App from './App.vue';
Vue.use(VueAxios, axios);

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

import './bootstrap'

// import '@fortawesome/fontawesome-free/css/all.css'
// import '@fortawesome/fontawesome-free/js/all.js'

// USER
import Index from './components/user/Index.vue';
import Visimisi from './components/user/Visimisi.vue';
import Prestasi from './components/user/Prestasi.vue';
import Fasilitas from './components/user/Fasilitas.vue';
import Pengajar from './components/user/Pengajar.vue';
import Ekskul from './components/user/Ekskul.vue';
import Berita from './components/user/Berita.vue';
import Detailberita from './components/user/Detailberita.vue';
import Pengumuman from './components/user/Pengumuman.vue';
import Detailpengumuman from './components/user/Detailpengumuman.vue';
import Galeri from './components/user/Galeri.vue';
import Kontak from './components/user/Kontak.vue';

// SUPERADMIN
import Login from './components/superadmin/Index.vue';

import Hero from './components/superadmin/hero/Hero.vue';
import TambahHero from './components/superadmin/hero/Tambah.vue';
import EditHero from './components/superadmin/hero/Edit.vue';

import Sambutan from './components/superadmin/sambutan/Sambutan.vue';
import EditSambutan from './components/superadmin/sambutan/Edit.vue';

import Video from './components/superadmin/video/Video.vue';
import EditVideo from './components/superadmin/video/Edit.vue';

import Alumni from './components/superadmin/alumni/Alumni.vue';
import TambahAlumni from './components/superadmin/alumni/Tambah.vue';
import EditAlumni from './components/superadmin/alumni/Edit.vue';

import Visi from './components/superadmin/visi/Visi.vue';
import EditVisi from './components/superadmin/visi/Edit.vue';

import Misi from './components/superadmin/misi/Misi.vue';
import TambahMisi from './components/superadmin/misi/Tambah.vue';
import EditMisi from './components/superadmin/misi/Edit.vue';

import PrestasiSuperadmin from './components/superadmin/prestasi/Prestasi.vue';
import TambahPrestasi from './components/superadmin/prestasi/Tambah.vue';
import EditPrestasi from './components/superadmin/prestasi/Edit.vue';

import FasilitasSuperadmin from './components/superadmin/fasilitas/Fasilitas.vue';
import TambahFasilitas from './components/superadmin/fasilitas/Tambah.vue';
import EditFasilitas from './components/superadmin/fasilitas/Edit.vue';

import PengajarSuperadmin from './components/superadmin/pengajar/Pengajar.vue';
import TambahPengajar from './components/superadmin/pengajar/Tambah.vue';
import EditPengajar from './components/superadmin/pengajar/Edit.vue';

import EskulSuperadmin from './components/superadmin/eskul/Eskul.vue';
import TambahEskul from './components/superadmin/eskul/Tambah.vue';
import EditEskul from './components/superadmin/eskul/Edit.vue';

import BeritaSuperadmin from './components/superadmin/berita/Berita.vue';
import TambahBerita from './components/superadmin/berita/Tambah.vue';
import EditBerita from './components/superadmin/berita/Edit.vue';
import DetailBerita from './components/superadmin/berita/Detail.vue';

import PengumumanSuperadmin from './components/superadmin/pengumuman/Pengumuman.vue';
import TambahPengumuman from './components/superadmin/pengumuman/Tambah.vue';
import EditPengumuman from './components/superadmin/pengumuman/Edit.vue';
import DetailPengumuman from './components/superadmin/pengumuman/Detail.vue';

import GaleriSuperadmin from './components/superadmin/galeri/Galeri.vue';
import TambahGaleri from './components/superadmin/galeri/Tambah.vue';
import EditGaleri from './components/superadmin/galeri/Edit.vue';

import KontakSuperadmin from './components/superadmin/kontak/Kontak.vue';
import EditKontak from './components/superadmin/kontak/Edit.vue';

import HubungiSuperadmin from './components/superadmin/hubungi/Hubungi.vue';

import PengaturanAdmin from './components/superadmin/pengaturan/Admin.vue';
import TambahPengaturan from './components/superadmin/pengaturan/Tambah.vue';
// import EditPengaturan from './components/superadmin/pengaturan/Edit.vue';

import PengaturanSuperadmin from './components/superadmin/superadmin/Admin.vue';
// import TambahPengaturanSuperadmin from './components/superadmin/superadmin/Tambah.vue';
// import EditPengaturanSuperadmin from './components/superadmin/superadmin/Edit.vue';

// ADMIN
import LoginAdmin from './components/admin/Index.vue';

import HeroAdmin from './components/admin/hero/Hero.vue';
import TambahHeroAdmin from './components/admin/hero/Tambah.vue';
import EditHeroAdmin from './components/admin/hero/Edit.vue';

import SambutanAdmin from './components/admin/sambutan/Sambutan.vue';
import EditSambutanAdmin from './components/admin/sambutan/Edit.vue';

import VideoAdmin from './components/admin/video/Video.vue';
import EditVideoAdmin from './components/admin/video/Edit.vue';

import AlumniAdmin from './components/admin/alumni/Alumni.vue';
import TambahAlumniAdmin from './components/admin/alumni/Tambah.vue';
import EditAlumniAdmin from './components/admin/alumni/Edit.vue';

import VisiAdmin from './components/admin/visi/Visi.vue';
import EditVisiAdmin from './components/admin/visi/Edit.vue';

import MisiAdmin from './components/admin/misi/Misi.vue';
import TambahMisiAdmin from './components/admin/misi/Tambah.vue';
import EditMisiAdmin from './components/admin/misi/Edit.vue';

import PrestasiAdmin from './components/admin/prestasi/Prestasi.vue';
import TambahPrestasiAdmin from './components/admin/prestasi/Tambah.vue';
import EditPrestasiAdmin from './components/admin/prestasi/Edit.vue';

import FasilitasAdmin from './components/admin/fasilitas/Fasilitas.vue';
import TambahFasilitasAdmin from './components/admin/fasilitas/Tambah.vue';
import EditFasilitasAdmin from './components/admin/fasilitas/Edit.vue';

import PengajarAdmin from './components/admin/pengajar/Pengajar.vue';
import TambahPengajarAdmin from './components/admin/pengajar/Tambah.vue';
import EditPengajarAdmin from './components/admin/pengajar/Edit.vue';

import EskulAdmin from './components/admin/eskul/Eskul.vue';
import TambahEskulAdmin from './components/admin/eskul/Tambah.vue';
import EditEskulAdmin from './components/admin/eskul/Edit.vue';

import BeritaAdmin from './components/admin/berita/Berita.vue';
import TambahBeritaAdmin from './components/admin/berita/Tambah.vue';
import EditBeritaAdmin from './components/admin/berita/Edit.vue';
import DetailBeritaAdmin from './components/admin/berita/Detail.vue';

import PengumumanAdmin from './components/admin/pengumuman/Pengumuman.vue';
import TambahPengumumanAdmin from './components/admin/pengumuman/Tambah.vue';
import EditPengumumanAdmin from './components/admin/pengumuman/Edit.vue';
import DetailPengumumanAdmin from './components/admin/pengumuman/Detail.vue';

import GaleriAdmin from './components/admin/galeri/Galeri.vue';
import TambahGaleriAdmin from './components/admin/galeri/Tambah.vue';
import EditGaleriAdmin from './components/admin/galeri/Edit.vue';

import KontakAdmin from './components/admin/kontak/Kontak.vue';
import EditKontakAdmin from './components/admin/kontak/Edit.vue';

import HubungiAdmin from './components/admin/hubungi/Hubungi.vue';

const routes = [
    // USER
    {
        name: 'beranda',
        path: '/',
        component: Index
    },
    {
        name: 'visimisi',
        path: '/visimisi-alhuda',
        component: Visimisi
    },
    {
        name: 'prestasi',
        path: '/prestasi-alhuda',
        component: Prestasi
    },
    {
        name: 'fasilitas',
        path: '/fasilitas-alhuda',
        component: Fasilitas
    },
    {
        name: 'pengajar',
        path: '/pengajar-alhuda',
        component: Pengajar
    },
    {
        name: 'eskul',
        path: '/ekstrakulikuler-alhuda',
        component: Ekskul
    },
    {
        name: 'berita',
        path: '/berita-alhuda',
        component: Berita
    },
    {
        name: 'detail-berita',
        path: '/detail-berita/:id',
        component: Detailberita
    },
    {
        name: 'pengumuman',
        path: '/pengumuman-alhuda',
        component: Pengumuman
    },
    {
        name: 'detail-pengumuman',
        path: '/detail-pengumuman/:id',
        component: Detailpengumuman
    },
    {
        name: 'galeri',
        path: '/galeri-alhuda',
        component: Galeri
    },
    {
        name: 'kontak',
        path: '/kontak-alhuda',
        component: Kontak
    },


    // SUPERADMIN
    {
        name: 'superadmin-login',
        path: '/superadmin',
        component: Login
    },

    // hero
    {
        name: 'superadmin-hero',
        path: '/superadmin/hero',
        component: Hero
    },
    {
        name: 'superadmin-hero-tambah',
        path: '/superadmin/hero/tambah',
        component: TambahHero
    },
    {
        name: 'superadmin-hero-edit',
        path: '/superadmin/hero/edit/:id',
        component: EditHero
    },
    
    // sambutan
    {
        name: 'superadmin-sambutan',
        path: '/superadmin/sambutan',
        component: Sambutan
    },
    {
        name: 'superadmin-sambutan-edit',
        path: '/superadmin/sambutan/edit/:id',
        component: EditSambutan
    },

    // video
    {
        name: 'superadmin-video',
        path: '/superadmin/video',
        component: Video
    },
    {
        name: 'superadmin-video-edit',
        path: '/superadmin/video/edit/:id',
        component: EditVideo
    },

    // alumni
    {
        name: 'superadmin-alumni',
        path: '/superadmin/alumni',
        component: Alumni
    },
    {
        name: 'superadmin-alumni-tambah',
        path: '/superadmin/alumni/tambah',
        component: TambahAlumni
    },
    {
        name: 'superadmin-alumni-edit',
        path: '/superadmin/alumni/edit/:id',
        component: EditAlumni
    },

    // visi
    {
        name: 'superadmin-visi',
        path: '/superadmin/visi',
        component: Visi
    },
    {
        name: 'superadmin-visi-edit',
        path: '/superadmin/visi/edit/:id',
        component: EditVisi
    },

    // misi
    {
        name: 'superadmin-misi',
        path: '/superadmin/misi',
        component: Misi
    },
    {
        name: 'superadmin-misi-tambah',
        path: '/superadmin/misi/tambah',
        component: TambahMisi
    },
    {
        name: 'superadmin-misi-edit',
        path: '/superadmin/misi/edit/:id',
        component: EditMisi
    },

    // prestasi
    {
        name: 'superadmin-prestasi',
        path: '/superadmin/prestasi',
        component: PrestasiSuperadmin
    },
    {
        name: 'superadmin-prestasi-tambah',
        path: '/superadmin/prestasi/tambah',
        component: TambahPrestasi
    },
    {
        name: 'superadmin-prestasi-edit',
        path: '/superadmin/prestasi/edit/:id',
        component: EditPrestasi
    },

    // fasilitas
    {
        name: 'superadmin-fasilitas',
        path: '/superadmin/fasilitas',
        component: FasilitasSuperadmin
    },
    {
        name: 'superadmin-fasilitas-tambah',
        path: '/superadmin/fasilitas/tambah',
        component: TambahFasilitas
    },
    {
        name: 'superadmin-fasilitas-edit',
        path: '/superadmin/fasilitas/edit/:id',
        component: EditFasilitas
    },

    // pengajar
    {
        name: 'superadmin-pengajar',
        path: '/superadmin/pengajar',
        component: PengajarSuperadmin
    },
    {
        name: 'superadmin-pengajar-tambah',
        path: '/superadmin/pengajar/tambah',
        component: TambahPengajar
    },
    {
        name: 'superadmin-pengajar-edit',
        path: '/superadmin/pengajar/edit/:id',
        component: EditPengajar
    },

    // eskul
    {
        name: 'superadmin-ekstrakulikuler',
        path: '/superadmin/ekstrakulikuler',
        component: EskulSuperadmin
    },
    {
        name: 'superadmin-ekstrakulikuler-tambah',
        path: '/superadmin/ekstrakulikuler/tambah',
        component: TambahEskul
    },
    {
        name: 'superadmin-esktrakulikuler-edit',
        path: '/superadmin/esktrakulikuler/edit/:id',
        component: EditEskul
    },

    // berita
    {
        name: 'superadmin-berita',
        path: '/superadmin/berita',
        component: BeritaSuperadmin
    },
    {
        name: 'superadmin-berita-tambah',
        path: '/superadmin/berita/tambah',
        component: TambahBerita
    },
    {
        name: 'superadmin-berita-edit',
        path: '/superadmin/berita/edit/:id',
        component: EditBerita
    },
    {
        name: 'superadmin-berita-detail',
        path: '/superadmin/berita/detail/:id',
        component: DetailBerita
    },

    // pengumuman
    {
        name: 'superadmin-pengumuman',
        path: '/superadmin/pengumuman',
        component: PengumumanSuperadmin
    },
    {
        name: 'superadmin-pengumuman-tambah',
        path: '/superadmin/pengumuman/tambah',
        component: TambahPengumuman
    },
    {
        name: 'superadmin-pengumuman-edit',
        path: '/superadmin/pengumuman/edit/:id',
        component: EditPengumuman
    },
    {
        name: 'superadmin-pengumuman-detail',
        path: '/superadmin/pengumuman/detail/:id',
        component: DetailPengumuman
    },

    // galeri
    {
        name: 'superadmin-galeri',
        path: '/superadmin/galeri',
        component: GaleriSuperadmin
    },
    {
        name: 'superadmin-galeri-tambah',
        path: '/superadmin/galeri/tambah',
        component: TambahGaleri
    },
    {
        name: 'superadmin-galeri-edit',
        path: '/superadmin/galeri/edit/:id',
        component: EditGaleri
    },

     // kontak
     {
        name: 'superadmin-kontak',
        path: '/superadmin/kontak',
        component: KontakSuperadmin
    },
    {
        name: 'superadmin-kontak-edit',
        path: '/superadmin/kontak/edit/:id',
        component: EditKontak
    },

    // hubungi
    {
        name: 'superadmin-hubungi',
        path: '/superadmin/hubungi',
        component: HubungiSuperadmin
    },

    // pengaturan admin
    {
        name: 'admin-pengaturan',
        path: '/admin/pengaturan',
        component: PengaturanAdmin
    },
    {
        name: 'admin-pengaturan-tambah',
        path: '/admin/pengaturan/tambah',
        component: TambahPengaturan
    },
    // {
    //     name: 'admin-pengaturan-edit',
    //     path: '/admin/pengaturan/edit',
    //     component: EditPengaturan
    // },

    // pengaturan admin
    {
        name: 'superadmin-pengaturan',
        path: '/superadmin/pengaturan',
        component: PengaturanSuperadmin
    },
    // {
    //     name: 'superadmin-pengaturan-tambah',
    //     path: '/superadmin/pengaturan/tambah',
    //     component: TambahPengaturanSuperadmin
    // },
    // {
    //     name: 'superadmin-pengaturan-edit',
    //     path: '/superadmin/pengaturan/edit',
    //     component: EditPengaturanSuperadmin
    // },

    // ADMIN
    {
        name: 'admin-login',
        path: '/admin',
        component: LoginAdmin
    },

    // hero
    {
        name: 'admin-hero',
        path: '/admin/hero',
        component: HeroAdmin
    },
    {
        name: 'admin-hero-tambah',
        path: '/admin/hero/tambah',
        component: TambahHeroAdmin
    },
    {
        name: 'admin-hero-edit',
        path: '/admin/hero/edit/:id',
        component: EditHeroAdmin
    },
     // sambutan
     {
        name: 'admin-sambutan',
        path: '/admin/sambutan',
        component: SambutanAdmin
    },
    {
        name: 'admin-sambutan-edit',
        path: '/admin/sambutan/edit/:id',
        component: EditSambutanAdmin
    },

     // video
     {
        name: 'admin-video',
        path: '/admin/video',
        component: VideoAdmin
    },
    {
        name: 'admin-video-edit',
        path: '/admin/video/edit/:id',
        component: EditVideoAdmin
    },

    // alumni
    {
        name: 'admin-alumni',
        path: '/admin/alumni',
        component: AlumniAdmin
    },
    {
        name: 'admin-alumni-tambah',
        path: '/admin/alumni/tambah',
        component: TambahAlumniAdmin
    },
    {
        name: 'admin-alumni-edit',
        path: '/admin/alumni/edit/:id',
        component: EditAlumniAdmin
    },
    // visi
    {
        name: 'admin-visi',
        path: '/admin/visi',
        component: VisiAdmin
    },
    {
        name: 'admin-visi-edit',
        path: '/admin/visi/edit/:id',
        component: EditVisiAdmin
    },

    // misi
    {
        name: 'admin-misi',
        path: '/admin/misi',
        component: MisiAdmin
    },
    {
        name: 'admin-misi-tambah',
        path: '/admin/misi/tambah',
        component: TambahMisiAdmin
    },
    {
        name: 'admin-misi-edit',
        path: '/admin/misi/edit/:id',
        component: EditMisiAdmin
    },

     // prestasi
     {
        name: 'admin-prestasi',
        path: '/admin/prestasi',
        component: PrestasiAdmin
    },
    {
        name: 'admin-prestasi-tambah',
        path: '/admin/prestasi/tambah',
        component: TambahPrestasiAdmin
    },
    {
        name: 'admin-prestasi-edit',
        path: '/admin/prestasi/edit/:id',
        component: EditPrestasiAdmin
    },

    // fasilitas
    {
        name: 'admin-fasilitas',
        path: '/admin/fasilitas',
        component: FasilitasAdmin
    },
    {
        name: 'admin-fasilitas-tambah',
        path: '/admin/fasilitas/tambah',
        component: TambahFasilitasAdmin
    },
    {
        name: 'admin-fasilitas-edit',
        path: '/admin/fasilitas/edit/:id',
        component: EditFasilitasAdmin
    },

    // pengajar
    {
        name: 'admin-pengajar',
        path: '/admin/pengajar',
        component: PengajarAdmin
    },
    {
        name: 'admin-pengajar-tambah',
        path: '/admin/pengajar/tambah',
        component: TambahPengajarAdmin
    },
    {
        name: 'admin-pengajar-edit',
        path: '/admin/pengajar/edit/:id',
        component: EditPengajarAdmin
    },

    // eskul
    {
        name: 'admin-ekstrakulikuler',
        path: '/admin/ekstrakulikuler',
        component: EskulAdmin
    },
    {
        name: 'admin-ekstrakulikuler-tambah',
        path: '/admin/ekstrakulikuler/tambah',
        component: TambahEskulAdmin
    },
    {
        name: 'admin-esktrakulikuler-edit',
        path: '/admin/esktrakulikuler/edit/:id',
        component: EditEskulAdmin
    },

    // berita
    {
        name: 'admin-berita',
        path: '/admin/berita',
        component: BeritaAdmin
    },
    {
        name: 'admin-berita-tambah',
        path: '/admin/berita/tambah',
        component: TambahBeritaAdmin
    },
    {
        name: 'admin-berita-edit',
        path: '/admin/berita/edit/:id',
        component: EditBeritaAdmin
    },
    {
        name: 'admin-berita-detail',
        path: '/admin/berita/detail/:id',
        component: DetailBeritaAdmin
    },

    // pengumuman
    {
        name: 'admin-pengumuman',
        path: '/admin/pengumuman',
        component: PengumumanAdmin
    },
    {
        name: 'admin-pengumuman-tambah',
        path: '/admin/pengumuman/tambah',
        component: TambahPengumumanAdmin
    },
    {
        name: 'admin-pengumuman-edit',
        path: '/admin/pengumuman/edit/:id',
        component: EditPengumumanAdmin
    },
    {
        name: 'admin-pengumuman-detail',
        path: '/admin/pengumuman/detail/:id',
        component: DetailPengumumanAdmin
    },

    // galeri
    {
        name: 'admin-galeri',
        path: '/admin/galeri',
        component: GaleriAdmin
    },
    {
        name: 'admin-galeri-tambah',
        path: '/admin/galeri/tambah',
        component: TambahGaleriAdmin
    },
    {
        name: 'admin-galeri-edit',
        path: '/admin/galeri/edit/:id',
        component: EditGaleriAdmin
    },

    // kontak
    {
        name: 'admin-kontak',
        path: '/admin/kontak',
        component: KontakAdmin
    },
    {
        name: 'admin-kontak-edit',
        path: '/admin/kontak/edit/:id',
        component: EditKontakAdmin
    },

     // hubungi
     {
        name: 'admin-hubungi',
        path: '/admin/hubungi',
        component: HubungiAdmin
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});

const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');
// const superadmin = new Vue(Vue.util.extend({ router }, App)).$mount('#superadmin');