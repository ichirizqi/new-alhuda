<html>
<head>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet"/>
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/fontawesome-free/css/all.min.css"> -->
    
    <!-- <script>
        tailwind.config = {
            theme: {
        extend: {
        fontFamily: {
            'inter': ['Inter', 'sans-serif'],
            'poppins': ['Poppins', 'sans-serif'],
            'dm-sans': ['DM Sans', 'sans-serif'],
            'montserrat': ['Montserrat', 'sans-serif'],
            'quicksand': ['Quicksand', 'sans-serif'],
            'worksans': ['Work Sans', 'sans-serif']
        },
        colors: {
            'primary-black': '#18191F',
            'primary-white': '#F7F7F7',
            'primary-green': '#00998f',
            'primary-blue': '#C7DEF0',
            'secondary-blue': '#005799',
            'ternary-blue': '#2E75AC',
            'fourth-blue': '#DDF0EF',
            'five-blue': '#03225F',
            'six-blue': '#1E0E62',
            'secondary-green' : '#026C62',
            'ternary-green': '#4DB3AD',
            'primary-tosca': '#5FBBB5',
            'primary-grey': '#EBEAED'
        },
        height: {
            '6px': '6px',
            '188px': '188px',
            '633px': '633px',
            '457px': '457px',
        },
        width: {
            '500px': '500px',
            '445px': '445px',
            '635px': '635px',
            '750px': '750px',
        },
        fontSize: {
            '44px': '44px',
            '40px': '40px'
        },
        borderRadius: {
            '55px': '55px'
        },
        margin: {
            '-500': '-50rem',
            '-600': '-60rem'
        },
        zIndex: {
            '-10': '-10',
            '-20': '-20',
            '-30': '-30',
            '-40': '-40',
            '-50': '-50'
        }
        },
     },
        }
    </script> -->
</head>
<body>
<div id="superadmin">
</div>
</body>

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</html>